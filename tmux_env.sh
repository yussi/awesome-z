#!/bin/bash
SESSION_NAME="awesome-z-dev"
tmux new-session -s $SESSION_NAME -n "rc" -d "vi rc.lua"
tmux new-window -t "$SESSION_NAME:1" -n "z.it.utils" -d "vi z/it/utils.lua"
tmux new-window -t "$SESSION_NAME:2" -n "elements" -d "vi z/it/elements.lua"
tmux new-window -t "$SESSION_NAME:3" -n "screen" -d "vi z/it/screen.lua"
tmux new-window -t "$SESSION_NAME:4" -n "tagset" -d "vi z/it/tagset.lua"
tmux new-window -t "$SESSION_NAME:5" -n "tag" -d "vi z/it/tag.lua"

tmux -2 attach-session -t $SESSION_NAME
