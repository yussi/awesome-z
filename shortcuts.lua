local z=z
local io=require("io")
local os=os
local awful=require("awful")
local table=table
module("shortcuts")
function open_all_lecture_notes(it,cmd,scr)
    if not it or not cmd or not scr then return -1 end
    local tag={}
    local mts={}
    mts.label="gameT"
    mts.tag={}
    for l in io.popen(cmd):lines() do
        local mtag={}
        mtag.label="lbl"
        mtag.commands={}
        mtag.commands[1]="xpdf '"..l.."'"
        z.debug.msg(mtag.commands[1])
        --local mtag_obj=z.it.tag(mtag,scr)
        table.insert(mts.tag,mtag)
    end
    local ts=z.it.tagset(mts,scr)
    it.screen[scr]:add_tagset({tagset=ts})
    it:bind_tagset_signals(it.screen[scr],ts)
end
